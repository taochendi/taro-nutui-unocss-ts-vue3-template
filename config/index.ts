import Components from 'unplugin-vue-components/webpack';
import AutoImport from 'unplugin-auto-import/webpack'
import NutUIResolver from '@nutui/nutui-taro/dist/resolver';

const commonChain = (chain) => {
  // https://github.com/antfu/unplugin-auto-import
  chain.plugin('unplugin-auto-import').use(AutoImport({
    imports: [
      'vue',
      'pinia',
    ],
    dts: 'types/auto-imports.d.ts',
    dirs: [
      'src/composables',
      'src/stores',
    ],
    vueTemplate: true,
  }))
  // 添加组件按需引入, 自动引入 `src/components` 目录下的组件
  // https://github.com/antfu/unplugin-vue-components
  chain.plugin('unplugin-vue-components').use(Components({
    dts: 'types/components.d.ts',
    dirs: ['src/components', 'src/layouts'],
    resolvers: [NutUIResolver({taro: true})]
  }))
  chain.merge({
    module: {
      rule: {
        mjsScript: {
          test: /\.mjs$/,
          include: [/pinia/, /unplugin-vue-components/, /unplugin-auto-import/],
          use: {
            babelLoader: {
              loader: require.resolve('babel-loader'),
            },
          },
        },
      },
    },
  })
}

const config = {
  projectName: 'taochen-taro-app-vue3',
  date: '2023-10-20',
  designWidth (input) {
    if (input?.file?.replace(/\\+/g, '/').indexOf('@nutui') > -1) {
      return 375
    }
    return 750
  },
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
    375: 2 / 1
  },
  sourceRoot: 'src',
  outputRoot: 'dist',
  plugins: [
    'taro-plugin-pinia',
    '@tarojs/plugin-html',
    '@tarojs/plugin-http',
    ['taro-plugin-unocss', {
      preset: {
        remToRpx: {
          baseFontSize: 4,
        },
      },
    }]],
  defineConstants: {
  },
  copy: {
    patterns: [
    ],
    options: {
    }
  },
  framework: 'vue3',
  compiler: {
    type: 'webpack5',
    prebundle: { enable: false }
  },
  sass:{
    data: `@import "@nutui/nutui-taro/dist/styles/variables-jdt.scss";`
  },
  mini: {
    webpackChain(chain) {
      commonChain(chain)
    },
    postcss: {
      pxtransform: {
        enable: true,
        config: {
          // selectorBlackList: ['nut-']
        }
      },
      url: {
        enable: true,
        config: {
          limit: 1024 // 设定转换尺寸上限
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    }
  },
  h5: {
    webpackChain(chain) {
      commonChain(chain)
    },
    publicPath: '/',
    staticDirectory: 'static',
    esnextModules: ['nutui-taro', 'icons-vue-taro'],
    postcss: {
      autoprefixer: {
        enable: true,
        config: {
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    }
  }
}

module.exports = function (merge) {
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  }
  return merge({}, config, require('./prod'))
}
