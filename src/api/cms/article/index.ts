import request from '../../../config/axios'

export interface ArticleVO {
  id: number
  title: string
  coverUrl: string
  categoryId: number
  categoryName: string
  keywords: string
  summary: string
  content: string
  author: string
  source: string
  isTop: number
  isRecommend: number
  praiseCount: number
  isComment: number
  commentCount: number
  readCount: number
  sort: number
  status: number
}

// 查询文章列表
export const getArticlePage = async (params) => {
  return await request.get({ url: `/cms/article/page`, params })
}

// 查询文章列表
export const getArticleList = async (params) => {
  return await request.get({ url: `/cms/article/list`, params })
}

// 查询文章详情
export const getArticle = async (id: number) => {
  return await request.get({ url: `/cms/article/get?id=` + id })
}


